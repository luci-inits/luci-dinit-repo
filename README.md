# Welcome to luci-dinit-repo
This repo contain binary packages for dinit services

# How to add this repo in pacman?

Append following in  `/etc/pacman.conf`
```
[luci-dinit-repo]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/luci-inits/$repo/-/raw/main/$arch
```

Update pacman
`sudo pacman -Syu`
